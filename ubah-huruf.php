<?php
function ubah_huruf($string){
	$huruf = "abcdefghijklmnopqrstuvwxyz";
	$ubah = "";
	for ($i=0; $i < strlen($string); $i++) {
		$position = strpos($huruf, $string[$i]);
		$ubah .= substr($huruf, $position + 1, 1);
	}
	return $ubah ."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>